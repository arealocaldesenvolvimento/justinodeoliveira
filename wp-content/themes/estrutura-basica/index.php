<?php get_header(); ?>
  <div class="faixa-branca">
    <div id="rev-home">
        <?= do_shortcode('[rev_slider alias="home"][/rev_slider]') ?>
    </div>
  </div>
  <section class="al-container home historia">
    <div class="left">
      <header>
        <span class="super-title">O ESCRITÓRIO</span>
        <h1 class="title">Conheça a<br>nossa história</h1>
      </header>
      <p>Justino de Oliveira Advogados está voltado à atuação consultiva e contenciosa altamente especializada e de excelência em direito público e arbitragem. Integrado por uma equipe multidisciplinar, realiza seus trabalhos com elevado grau de sofisticação técnica, privilegiando o atendimento a seus clientes por meio da busca de  soluções  inovadoras e efetivas, reflexo da sólida formação acadêmica de seu fundador, Prof. Dr. Gustavo Justino de Oliveira.</p>
      <a href="">conheça a empresa</a>
    </div>
    <div class="right">
      <img src="<?= get_image_url('justica.png') ?>" alt="Justiça">
    </div>
  </section>
  <div class="back-azul">
    <section class="al-container home atuacao">
      <h1>Área de atuação</h1>
      <div class="areas-atuacao">
      <?php $atuacao = new WP_Query(array(
          'post_type' => 'area_de_atuacao',
          'posts_per_page' => -1,
          'orderby' => 'date',
          'order' => 'ASC'
      ));
      while($atuacao->have_posts()) : $atuacao->the_post(); ?>
      <div class="article-container">
          <article class="area" id="id-<?php the_ID(); ?>">
              <!-- <a href="<?= get_the_permalink() ?>" title="<?=  $post->post_title ?>"> -->
                  <div class="img-container">
                      <img src="<?=  get_field('icone')['url'] ?>" alt="<?=  $post->post_title ?>"/>
                  </div>
                  <header>
                      <h3><?= get_the_title() ?></h3>
                  </header>
              <!-- </a> -->
          </article>
      </div>
      <?php endwhile; ?>
    </section>
  </div>
  <div class="fundo"></div>
  <section class="al-container home fundador">
    <div class="left">
      <header>
        <span class="super-title">SÓCIO-FUNDADOR</span>
        <h1 class="title">Prof. Gustavo Justino<br>de Oliveira</h1>
      </header>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vel lacus vel mauris efficitur rhoncus. Nunc vel interdum lacus. Phasellus molestie mauris sed lacus tempus congue. Nullam tincidunt bibendum mauris, sed viverra ipsum suscipit at. In metus metus, vestibulum vel congue a, ullamcorper vitae magna. Nam aliquam mi auctor accumsan iaculis. Suspendisse eget mollis sapien. Phasellus semper pellentesque turpis et porttitor. Donec a dapibus urna. Donec ornare sapien metus, vehicula maximus urna vehicula in. Nunc nec mollis lorem. Curabitur volutpat lorem at orci pellentesque tincidunt.</p>
      <a href="">conheça a equipe</a>
    </div>
    <div class="right">
      <img src="<?= get_image_url('fundador.jpg') ?>" alt="Sócio Fundador">
    </div>
  </section>
  <section class="al-container home blog">
    <header>
        <span class="super-title">ÚLTIMAS NOTICIAS</span>
        <h1 class="title">Entenda o que esta<br>acontecendo na área jurídica</h1>
    </header>
    <div class="entry-content-blog">
        <div id="novidades-blog" class="content-slider">
            <?php $blog = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => 6,
                'orderby' => 'date',
                'order' => 'desc'
            ));
            while($blog->have_posts()) : $blog->the_post(); ?>
            <div class="article-container">
                <article class="clearfix" id="id-<?php the_ID(); ?>">
                    <a href="<?= get_the_permalink() ?>" title="<?=  $post->post_title ?>">
                        <span class="data"><b><?= date_i18n('j') ?></b><br><?= date_i18n('M') ?></span>
                        <img src="<?=  get_thumbnail_url(get_the_ID(), 'large') ?>" alt="<?=  $post->post_title ?>"/>
                    </a>
                    <header>
                      <div class="caption">
                        <span class="categoria"><?= get_the_category()[0]->name ?></span>
                      </div>
                      <h2><a href="<?= get_permalink() ?>"><?= get_the_title() ?></a></h2>
                    </header>
                    <p class="leia-mais"><a href="<?= get_permalink() ?>">Ler mais <i class="fas fa-arrow-right"></i></a></p>
                </article>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
  </section>
<?php get_footer(); ?>
