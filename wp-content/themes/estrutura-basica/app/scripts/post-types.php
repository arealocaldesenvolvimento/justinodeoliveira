<?php

flush_rewrite_rules();

function area_de_atuacao()
{
    register_post_type('area_de_atuacao', [
        'labels' => [
            'name' => _x('Áreas de atuação', 'post type general name'),
            'singular_name' => _x('Área de atuação', 'post type singular name'),
            'add_new' => __('Adicionar Área de atuação'),
            'add_new_item' => __('Adicionar Área de atuação'),
            'edit_item' => __('Editar Áreas de atuação'),
            'new_item' => __('Novo Área de atuação'),
            'view_item' => __('Ver Áreas de atuação'),
            'not_found' =>  __('Nenhum Área de atuação encontrada'),
            'not_found_in_trash' => __('Nenhum Área de atuação encontrada na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Áreas de atuação'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-media-document',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title','editor','thumbnail','custom-fields', 'revisions']
    ]);
}
add_action('init', 'area_de_atuacao');
 