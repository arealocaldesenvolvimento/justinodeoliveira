/**
 * Global theme functions
 */

/**
 * @description Exemplo ajax
 */
const exampleAjax = async () => {
	const stateResponse = await fetch(`${window.apiUrl}/estados`)
	const { states, message } = await stateResponse.json()
	console.log(states, message)
}

window.onload = async () => {
	/**
	 * Contact form 7 alerts
	 */
	const form = document.querySelector('.wpcf7')
	if (form) {
		form.addEventListener('wpcf7mailsent', () => {
			Swal.fire({
				icon: 'success',
				title: 'Sucesso!',
				text: 'Mensagem enviada!',
			})
		})

		form.addEventListener('wpcf7mailfailed', () => {
			Swal.fire({
				icon: 'error',
				title: 'Ocorreu um erro!',
				text: 'Se o erro persistir, favor contatar o suporte.',
			})
		})
	}

	var width = $(window).width()
    var blog = 3;
    // var marca = 4;
    // var produto = 5;
    // if (width > 1900){
    //     produto = 6;
    // }
    // if (width < 1080){
    // produto = 4;
    // }
    if (width < 780) {
    // produto = 3;
    // marca = 3;
    blog = 2;
    }
    if (width < 480) {
    // produto = 1;
    // marca = 1;
    blog = 1;
    }

    if($('.content-slider').length){
        var slider_blog = tns({
            "container": ".content-slider",
            "controlsText": ['',''],
            "items": blog,
			"gutter": 30,
			"loop": true,
            "mouseDrag": true,
            "swipeAngle": false,
            "speed": 400
        });
    }

    $(".main-header .menu-bars").on( "click", function() {
        if ($('.menu-occ.active').length > 0) {
            $('.menu-occ').removeClass('active');
            $(".main-header .menu-bars").removeClass('active');
            $(".main-header .menu-bars .fa-times").fadeOut(300);
            $(".main-header .menu-bars .fa-bars").delay(300).fadeIn(300);
        } else {
            $('.menu-occ').addClass('active');
            $(".main-header .menu-bars").addClass('active');
            $(".main-header .menu-bars");
            $(".main-header .menu-bars .fa-bars").fadeOut(300);
            $(".main-header .menu-bars .fa-times").delay(300).fadeIn(300);
        }
    });
}

