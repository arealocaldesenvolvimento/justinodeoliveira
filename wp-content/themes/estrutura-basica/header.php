<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="web_author" content="Área Local">
    <meta name="theme-color" content="#000000">
    <link rel="icon" href="<?php image_url('favicon.png'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
    <link
        rel="stylesheet"
        type="text/css"
        media="all"
        href="<?php echo get_template_directory_uri(); ?>/style.css"
    >
    <title>
        <?php echo is_front_page() || is_home() ? get_bloginfo('name') : wp_title(''); ?>
    </title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <!-- Header -->
    <header role="heading" class="main-header">
        <div class="left">
            <div class="img-container">
                <img src="<?= get_image_url('logo.jpg') ?>" alt="Justino de Oliveira Advogados">
            </div>
        </div>
        <div class="right">
            <div class="img-container">
                <img src="<?= get_image_url('logo-branca.png') ?>" alt="Justino de Oliveira Advogados">
            </div>
            <div div class="menu-container">
                <div class="linguagem">
                    <a href="">BR</a>
                    <span> | </span>
                    <a href="">EN</a>
                </div>
                <div class="menu-bars">
                    <i class="fas fa-bars"></i>
                    <i class="fas fa-times"></i>
                </div>
            </div>
        </div>
        <div class="menu-occ">
            <div class="institucional left">
                <h2>Institucional</h2>
                <?php wp_nav_menu(array('menu'=> 'Institucional', 'theme_location' => 'principal', 'container' => false)); ?>
            </div>
            <div class="servicos right">
                <h2>Serviços</h2>
                <?php wp_nav_menu(array('menu'=> 'Serviços', 'theme_location' => 'principal', 'container' => false)); ?>
            </div>
        </div>
    </header>
    <!-- Wrapper -->
    <div id="wrapper">
