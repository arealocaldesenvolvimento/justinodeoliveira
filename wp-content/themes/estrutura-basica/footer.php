    </div>

    <!-- Footer -->
    <footer>
        <div class="footer-container">
            <footer id="main-footer" class="al-container">
                <div class="contato left">
                    <div class="wrap-logo">
                        <a href="<?= get_home_url(); ?>" id="header-logo">
                            <img src="<?= get_image_url('logo-branca.png') ?>" alt="Justino de Oliveira Advogados">
                        </a>
                    </div>
                    <div class="mail">
                        <a href="mailto:databox@databoxsistemas.com.br">contato@justinodeoliveira.com.br</a><br>
                        <a href="tel:+551135257274">+55 (11) 3525-7274</a>
                    </div>
                    <div class="social">
                        <a class="sociais" target="blank" href="https://www.facebook.com/justinodeoliveiraadvogados/">
                        <i class="fab fa-facebook-f"></i></a>
                        <a class="sociais" target="blank" href="https://api.whatsapp.com/send?phone=551135257274">
                        <i class="fab fa-whatsapp"></i></a>
                        <a class="sociais" target="blank" href="https://br.linkedin.com/company/justinodeoliveiraadvogados">
                        <i class="fab fa-linkedin-in"></i></a>
                        <a class="sociais" target="blank" href="https://www.youtube.com/channel/UCvXFFzmK0o5F42HUoTHyRrA">
                        <i class="fab fa-youtube"></i></a>
                    </div>
                </div>
                <div class="right">
                    <div class="institucional left">
                        <h2>Institucional</h2>
                        <?php wp_nav_menu(array('menu'=> 'Institucional', 'theme_location' => 'principal', 'container' => false)); ?>
                    </div>
                    <div class="servicos right">
                        <h2>Serviços</h2>
                        <?php wp_nav_menu(array('menu'=> 'Serviços', 'theme_location' => 'principal', 'container' => false)); ?>
                    </div>
                    <div class="newsletter">
                        <h2>Receba a nossa Newsletter</h2>
                        <?= do_shortcode('[contact-form-7 id="47" title="Newsletter"]') ?>
                    </div>
                </div>
            </footer>
        </div>
        <div class="second footer-container">
            <footer class="al-small-container">
                <div class="area-local">
                    <!-- <small>© Todos os direitos reservados - Databox - 2020 </small> -->
                    <a href="http://www.arealocal.com.br" target="_blank" title="Área Local">Desenvolvido por: Área Local</a>
                </div>
            </footer>
        </div>
    </footer>

    <!-- Scripts -->
    <script>
        /**
         * @description global JS variables
         */
        window.alUrl = {
            templateUrl: '<?php echo addslashes(get_bloginfo('template_url')); ?>',
            homeUrl: '<?php echo addslashes(home_url()); ?>'
        }
	    window.apiUrl = `${window.alUrl.homeUrl}/wp-json/api`
    </script>
    <script
        async
        type="text/javascript"
        src="<?php echo get_template_directory_uri(); ?>/public/js/vendor.js"
    ></script>
    <script
        async
        type="text/javascript"
        src="<?php echo get_template_directory_uri(); ?>/public/js/app.js"
    ></script>
    <?php wp_footer(); ?>
</body>
</html>
